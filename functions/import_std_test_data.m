function stdtestdatabosch700012LWM = import_std_test_data(workbookFile, sheetName, dataLines)
%IMPORTFILE Import data from a spreadsheet
%  STDTESTDATABOSCH700012LWM = IMPORTFILE(FILE) reads data from the
%  first worksheet in the Microsoft Excel spreadsheet file named FILE.
%  Returns the data as a table.
%
%  STDTESTDATABOSCH700012LWM = IMPORTFILE(FILE, SHEET) reads from the
%  specified worksheet.
%
%  STDTESTDATABOSCH700012LWM = IMPORTFILE(FILE, SHEET, DATALINES) reads
%  from the specified worksheet for the specified row interval(s).
%  Specify DATALINES as a positive scalar integer or a N-by-2 array of
%  positive scalar integers for dis-contiguous row intervals.
%
%  Example:
%  stdtestdatabosch700012LWM = importfile("C:\Users\MW86ZB\OneDrive - Aalborg Universitet\Dokumenter\Work\Phd\documentation\01_papers\cop_from_std_tests_cold_climate_hvac_2020\data\std_test_data_bosch_7000_12_LWM.xlsx", "Sheet1", [3, 18]);
%
%  See also READTABLE.
%
% Auto-generated by MATLAB on 16-Jan-2021 15:23:58

%% Input handling

% If no sheet is specified, read first sheet
if nargin == 1 || isempty(sheetName)
    sheetName = 1;
end

% If row start and end points are not specified, define defaults
if nargin <= 2
    dataLines = [3, 18];
end

%% Setup the Import Options and import the data
opts = spreadsheetImportOptions("NumVariables", 12);

% Specify sheet and range
opts.Sheet = sheetName;
opts.DataRange = "A" + dataLines(1, 1) + ":L" + dataLines(1, 2);

% Specify column names and types
opts.VariableNames = ["VarName1", "Q_HP_kW", "P_HP_kW", "T_F", "T_R", "flow", "mdot", "T_B_i", "T_B_o", "COP", "Q_HP", "P_HP"];
opts.VariableTypes = ["double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double"];

% Import the data
stdtestdatabosch700012LWM = readtable(workbookFile, opts, "UseExcel", false);

for idx = 2:size(dataLines, 1)
    opts.DataRange = "A" + dataLines(idx, 1) + ":L" + dataLines(idx, 2);
    tb = readtable(workbookFile, opts, "UseExcel", false);
    stdtestdatabosch700012LWM = [stdtestdatabosch700012LWM; tb]; %#ok<AGROW>
end

end