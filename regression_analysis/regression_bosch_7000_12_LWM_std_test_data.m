clc
close all
clear all

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DATA CONFIG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

USE_DATASET_FROM_PAPER = false;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IMPORT DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath('..\functions')

path = "..\data\baseline_OPSYSv1.xlsx";
path = "..\data\data_OPSYSv1.xlsx";


if USE_DATASET_FROM_PAPER
    % Dataset from paper
    path_std = "..\data\Bosch 7000 LWM.xlsx";
else
    % Cleaned dataset (Conclusions are similar)
    path_std = "..\data\std_test_data_bosch_7000_12_LWM.xlsx";
end
data1 = import_opsys1_data(path, "std");
data2 = import_opsys1_data_agilent(path);

if USE_DATASET_FROM_PAPER
    T_std = importfile_std_7000LWM(path_std, "ARK1");
else
    T_std = importfile_std_data_7000LWM("..\data\std_test_data_bosch_7000_12_LWM.xlsx");
end
disp("# Data imported")

dt = 60;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Prepare data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T = join(data1,data2,'Keys','day_num');
T.dt = [(T.day_num(2:end)-T.day_num(1:end-1))*86400; 60]; % Time difference

% Remove jump in data, but keep original
T_org = T(1:34500,:);

% Clean according to COP
T(isnan(T.COP),:) = []; % Reduce dataset where COP is nan
T.m_tot(T.COP==0) = 0;  % Set flow to zero where COP is zero
T.Q_HP(T.COP==0) = 0;   % Set heat flow to zero where COP is zero
T.COP(T.COP==0) = 1;    % Set COP = 0 to COP = 1

% Reduced set
T_red = T_org;
T_red(T_red.COP==0,:) = [];             % Remove  COP = 0
T_red(isnan(T_red.COP),:) = [];         % Remove rows where COP = nan
T_red(T_red.COP>5,:) = [];              % Remove rows where COP > 0

% Rename the varibles
T_red.Q_HP = 1000*T_red.Q_HP;           % Change kW to W
T_red.HP_on = T_red.Q_HP > 1000;        % Get logic-vector for HP on
T_red.P_HP = T_red.P_HP_kW*1000;        % Import power consumption
T_red.T_B = T_red.T_BR;                 % Brine Temperature
T_red.T_R = T_red.T_R_1;                % Pich return temperature from one sensor
T_red.mdot = T_red.m_tot;               % Change name on flow

T_red.u1 = T_red.m1 > 0.01;
T_red.u2 = T_red.m2 > 0.01;
T_red.u3 = T_red.m3 > 0.01;
T_red.u4 = T_red.m4 > 0.01;

% Data from standard test
if USE_DATASET_FROM_PAPER
    T_std.P_HP = T_std.P_HP_kW*1000;     % Power consumption from HP
    T_std.Q_HP = T_std.Q_HP_kW*1000;     % Heat production from HP
    T_std.T_F = T_std.T_FB;              % Forward temperature from Heat pump
    T_std.T_B = T_std.T_FB - T_std.T_FB; % Brine temperarature
    T_std.T_R = T_std.T_F - T_std.T_FR;  % Return temperature
else
    T_std.P_HP = T_std.P_HP_kW*1000;     % Power consumption from HP
    T_std.Q_HP = T_std.Q_HP_kW*1000;     % Heat production from HP
    T_std.T_F  = T_std.T_F;              % Forward temperature from Heat pump
    T_std.T_B  = T_std.T_B_i; % Brine temperarature
    T_std.T_R  = T_std.T_R;  % Return temperature
end
% Divide data into fit and 
days_fit = 10;
T_fit = T_red(1:days_fit*24*60,:);      % Online fit data
T_rec = T_red(days_fit*24*60+1:end,:);  % Data for validation and recursive fit


% Divide recursive test data into 2 groups
% 1. T_rec_fit: For fitting
% 2. T_rec_val: For validation

N = size(T_rec,1);
Q = floor(4*N/5);

T_rec_val = T_rec(Q+1:end,:);
T_rec_fit = T_rec(1:Q,:);
T_rec_val.P_HP_clean = set_negative_and_off_zero(T_rec_val.P_HP, T_rec_val.HP_on);


%% Regression model without restraints on data
close all
bias = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% P_HP = k0 + k1*T_F+c2*Q_HP+k3*COP_carnot
%           +k4*T_F^2+k5*Q_HP^2+k6*COP_carnot^2
% COP = (T_F+273.15)/(T_F-T_B)
% Q_HP = c*mdot*(T_F-T_R)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

k_best = create_regression(@std_reg_TF_TR_TB_mdot, T_fit, T_rec_val, bias)

[A_best, ~] = std_reg_TF_TR_TB_mdot(T_rec_val);
P_HP_hat = A_best*k_best;

% Only avaluate data where the heat pump is on
P_HP_hat = set_negative_and_off_zero(P_HP_hat, T_rec_val.HP_on);
P_HP = set_negative_and_off_zero(T_rec_val.P_HP, T_rec_val.HP_on);


plot_compare(T_rec_val.day_num, P_HP_hat, P_HP)

direct_RMSE = RMSE(P_HP_hat-T_rec_val.P_HP)


%% The regression based on standard test data P_HP = f(T_F, T_R, mdot)
clc
close all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% P_HP = k0 + k1*T_F + k2*T_R + k3*mdot +
%           + k4*T_F^2 + k5*T_R^2 + k6*mdot^2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(0,'defaultAxesFontSize',24);
set(0,'DefaultLineMarkerSize',10);
set(0,'defaultLineLineWidth',1.5);

bias = 0;

k_m = create_regression(@std_reg_TF_TR_m_sep, T_std, T_rec_val, bias)
    
% Estimate mass flow
[~, mdot_p] = mass_flow_est_2_2_1(T_red, k_m, bias);

[A_std_org, ~] = std_reg_TF_TR_m_sep(T_std);
P_HP_hat_std = A_std_org*k_m;

% The results for the data compared to estimate
disp('Results from comparing with input data')
r_square_std =  r_squared(P_HP_hat_std, T_std.P_HP)
direct_RMSE_std = RMSE(P_HP_hat_std-T_std.P_HP)

% Create estimates for validation data
[A_std, ~] = std_reg_TF_TR_m_sep(T_rec_val);
P_HP_hat_std_val = A_std*k_m;
P_HP_hat_std_val = set_negative_and_off_zero(P_HP_hat_std_val, T_rec_val.HP_on);

%P_HP = set_negative_and_off_zero(T_rec_val.P_HP, T_rec_val.HP_on);

% Calculate the flow using first 10 days of data.
[~, mdot_p] = mass_flow_est_2_2_1(T_fit, k_m, 0);
mdot_fit = real(mdot_p); % Make complex values real
mdot_fit(mdot_fit < 0) = 0;
T_fit.mdot_hat = mdot_fit;

% fit mdot_hat = k_1*u1 + ... + k_1*uM
[A_u, b_u] = reg_mdot_u(T_fit);
k_u = A_u\b_u;
mdot_fit = A_u*k_u;

% Evaluate the estimimate based on control signal
[A, y] = create_recursive_data_m_2(k_m, k_u ,T_rec_val);
P_HP_hat_u = A*k_m;
P_HP_hat_u = set_negative_and_off_zero(P_HP_hat_u, T_rec_val.HP_on);

disp('Results from validation data')
r_square_u =  r_squared(P_HP_hat_u, T_rec_val.P_HP_clean)
direct_RMSE_u = RMSE(P_HP_hat_u-T_rec_val.P_HP_clean)

% Plot result
plot_comparison_diag_2(P_HP_hat_std, T_std.P_HP, P_HP_hat_std_val, T_rec_val.P_HP, {'Fit data', 'Validation data'}, 'Regression based on Standard Test Data')
plot_name = 'standard_comp'
plot_size_w = 30;
plot_size_h = 12;
save_plot(plot_name, plot_size_w, plot_size_h)

plot_compare(T_rec_val.day_num, P_HP_hat_std_val, T_rec_val.P_HP_clean)

r_square_val =  r_squared(P_HP_hat_std_val, T_rec_val.P_HP_clean)
direct_RMSE = RMSE(P_HP_hat_std_val-T_rec_val.P_HP_clean)

%% Show flow comparison

T_flow = T_fit;
mdot_fit = set_negative_and_off_zero(mdot_fit, T_flow.HP_on);
T_flow.mdot_hat = set_negative_and_off_zero(T_flow.mdot_hat, T_flow.HP_on);
T_flow.mdot = set_negative_and_off_zero(T_flow.mdot, T_flow.HP_on);

% Plot result
set(0,'defaultAxesFontSize',24);
set(0,'DefaultLineMarkerSize',10);
set(0,'defaultLineLineWidth',1.5);

figure
tiledlayout(1,1, 'Padding', 'compact', 'TileSpacing', 'compact');
nexttile
hold on
plot(T_flow.mdot_hat)
plot(T_flow.mdot, 'linewidth', 5)
plot(mdot_fit, 'linewidth', 3)
legend({'$\hat{q}_{+}$','q Meas.','$\hat{q}_v$'}, 'interpreter', 'Latex', 'location', 'Southwest', 'fontsize', 22)
grid on
set(gca,'xlim', [5000 9000])
ylabel('Mass flow [kg/s]')
xlabel('Sample')
title('Measured and Estimated flow')
plot_size_w = 30;
plot_size_h = 13;
save_plot('flow_investigation', plot_size_w, plot_size_h)


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (RLS) Recusive fit using data from test-rig
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
close all

M = length(k_m);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CONFIG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Staring value for P
deltas = [1e-5 1e6];

% Staring value for alpha_hat
alpha_hat_0 = k_m;
%alpha_hat_0 = zeros(size(k_m)) % Initial with 0


num_delta = length(deltas);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INIT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

alpha_hat = zeros(M,Q,num_delta);
for i = 1:num_delta
    alpha_hat(:,1,i) = alpha_hat_0;
end
% The measurements we would have gathered
[A, y] = create_recursive_data_m_2(k_m, k_u ,T_rec_fit);
[A_val, y_val] = create_recursive_data_m_2(k_m, k_u ,T_rec_val);

P_HP = T_rec_val.P_HP;
P_HP = set_negative_and_off_zero(P_HP, T_rec_val.HP_on);

P_HP_hat_final = zeros(length(P_HP), num_delta);

rec_RMSE = zeros(Q,num_delta);
org_RMSE = zeros(Q,num_delta);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Recursive least squares (RLS)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i = 1:num_delta
    P0 = deltas(i)*eye(M);
    %g = zeros(M,Q);
    P = zeros(M,M,Q);
    P(:,:,1) = P0;
    for k = 2:Q
        % If the system is steady enough, then update
        if (T_rec_fit.HP_on(k) == 1)
            xk = A(k,:)';
            P(:,:,k) = P(:,:,k-1)-P(:,:,k-1)*(xk*xk')*P(:,:,k-1)/(1+xk'*P(:,:,k-1)*xk);
            alpha_hat(:,k,i) = alpha_hat(:,k-1,i)+P(:,:,k)*xk*(y(k)-xk'*alpha_hat(:,k-1,i))/(1+xk'*P(:,:,k-1)*xk);

        else % No update this time around 
            xk = A(k,:)';
            %g(:,k) = g(:,k-1);
            P(:,:,k) = P(:,:,k-1);
            P_bar = 1/1*P(:,:,k);
            alpha_hat(:,k,i) = alpha_hat(:,k-1,i);
        end
        P_HP_hat = A_val*alpha_hat(:,k,i);
        P_HP_hat = set_negative_and_off_zero(P_HP_hat, T_rec_val.HP_on);
        rec_RMSE(k,i) = RMSE(P_HP_hat-P_HP);

        P_HP_hat_org = A_val*k_m;
        P_HP_hat_org = set_negative_and_off_zero(P_HP_hat_org, T_rec_val.HP_on);
        org_RMSE(k,i) = RMSE(P_HP_hat_org-P_HP);
    end
    
    P_HP_hat_final(:,i) = P_HP_hat;
    
end
save('alpha_hat.mat', 'alpha_hat')
Percentage_improvement = abs(rec_RMSE(end)-org_RMSE(end))/org_RMSE(end)

alpha_hat_full = alpha_hat;



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
set(0,'defaultAxesFontSize',24);
set(0,'DefaultLineMarkerSize',10);
set(0,'defaultLineLineWidth',1.8);

figure
tiledlayout(1,2, 'Padding', 'compact', 'TileSpacing', 'compact');
nexttile
hold on
for i = 1:num_delta
    plot(T_rec_fit.day_num(2:end), rec_RMSE(2:end,i))
end
plot(T_rec_fit.day_num(2:end), org_RMSE(2:end,1))
set(gca, 'ylim', [0 max(max(rec_RMSE))])
set(gca, 'xlim', [T_rec_fit.day_num(2) T_rec_fit.day_num(end)])
title('All iterations')
legend({'Rec. Fit (\delta=10^{-5})','Rec. Fit (\delta=10^{6})','Org. Fit'}, 'location', 'northeast')
ylabel('RMSE [W] (Log scale)')
xlabel('Time [days]')
set(gca, 'YScale', 'log')
grid on

x_start = 3000
nexttile
hold on
for i = 1:num_delta
    plot(T_rec_fit.day_num(x_start:end), rec_RMSE(x_start:end,i))
    last_RMSE = rec_RMSE(end,i)
end
plot(T_rec_fit.day_num(x_start:end), org_RMSE(x_start:end,1))
set(gca, 'ylim', [0 max(max(org_RMSE(x_start:end,:)))+2])
set(gca, 'xlim', [T_rec_fit.day_num(x_start) T_rec_fit.day_num(end)])
title('Last iterations')
legend({'Rec. Fit (\delta=10^{-5})','Rec. Fit (\delta=10^{6})','Org. Fit'}, 'location', 'southeast')
ylabel('RMSE [W]')
xlabel('Time [days]')
grid on

sg = sgtitle('RMSE of estimate over the recursive fit period');
sg.FontSize = get(groot, 'DefaultAxesFontSize')*1.1;

save_plot('rec_pred', 30, 12)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Consumption of validation data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
E_HP_tot = sum(W2Kwh(T_rec_val.P_HP, dt));
err_P_1ep6 = P_HP - P_HP_hat_final(:,2);
err_P_1en5 = P_HP - P_HP_hat_final(:,1);
err_org = P_HP - P_HP_hat_std_val;

err_E_1ep6_tot = sum(abs(W2Kwh(err_P_1ep6, dt)));
err_E_1en5_tot = sum(abs(W2Kwh(err_P_1en5, dt)));
err_E_org = sum(abs(W2Kwh(err_org, dt)));

disp('Relative errors')
disp('delta = 1e6')
rel_error_1ep6 = err_E_1ep6_tot/E_HP_tot*100
disp('delta = 1e-5')
rel_error_1en5 = err_E_1en5_tot/E_HP_tot*100
disp('Fit based on standard test data')
rel_error_org = err_E_org/E_HP_tot*100

%%
set(0,'defaultAxesFontSize',14);
set(0,'DefaultLineMarkerSize',10);
set(0,'defaultLineLineWidth',1.5);
legs = {{'Org. Fit', 'Rec. Fit (\delta=10^{-5})','Rec. Fit (\delta=10^{6})', 'Meas'}, {'(Org. Fit)-Meas', '(Rec. Fit (\delta=10^{-5}))-Meas','(Rec. Fit (\delta=10^{6}))-Meas'}, {'Org. Fit', 'Rec. Fit (\delta=10^{-5})','Rec. Fit (\delta=10^{6})'}}
plot_compare_mult(T_rec_val.day_num, [P_HP_hat_std_val P_HP_hat_final], P_HP, legs)
plot_name = 'rec_result';

save_plot(plot_name, 35, 15)

%%
figure
plot(T_rec_fit.day_num, alpha_hat(:,:,1)')
set(gca, 'xlim', [T_rec_fit.day_num(1) T_rec_fit.day_num(end)])
legend({'C','T_F','T_R','q','T_{F}^2','T_R^2','q^2'},'Orientation','horizontal', 'location', 'southeast');
grid on
title('Develpment of coefficients over time')
% Plot result
xlabel('Time [days]')
ylabel('sign(c_i(t))Log10(c_i(t))')
plot_name = 'recursive_coef';
set(gcf, 'PaperOrientation', 'landscape'); %Set the paper to have width 5 and height 5.
set(gcf, 'PaperPositionMode', 'manual');

set(gcf, 'PaperPosition', [-2 0 30 10]); %[left bottom width height].

set(gcf, 'PaperSize', [28 8]); % [width height].

plot_name = char(plot_name);
saveas(gcf, plot_name, 'pdf') %Save figure
saveas(gcf, plot_name, 'png') %Save figure


%% (RLS) Investigate meaning of coefficients (10^-5, 10^6)
close all

y_limits = [-2000 3000; -4000 9000];
delta_str = {'10^{-5}', '10^{6}'};
xlabel_T  = 'T [\circC]';
xlabel_q  = 'q [kg/s]';

for s = 1:num_delta
    y_limit = y_limits(s,:);
    alpha_hat = alpha_hat_full(:,:,s);
    set(0,'defaultAxesFontSize',24);
    set(0,'DefaultLineMarkerSize',10);
    set(0,'defaultLineLineWidth',0.5);

    SIGN_ON = false;

    sign_c = 0.1;
    main_width = 2.5;
    coef_width = 2.5;
    coef_linestyle = '--';

    num_rec_itr = size(alpha_hat,2);
    num_steps = 10;

    color_num = colormap(gray);
    color_num = flip(color_num);
    len_color =size(color_num,1);

    tiledlayout(3,3, 'Padding', 'compact', 'TileSpacing', 'compact');

    % Forward Temperature
    first_order = 2;
    second_order = 5;
    T_F_min = 25;
    T_F_max = 60;
    T_F_sw = linspace(T_F_min, T_F_max, num_steps);
    T_F_cost = alpha_hat(first_order,:)'*T_F_sw + alpha_hat(second_order,:)'*T_F_sw.^2;

    nexttile(1, [2 1])
    hold on
    for i = 1:len_color-1
        plot(T_F_sw,T_F_cost(round(i*num_rec_itr/len_color),:), 'color', color_num(i,:),'HandleVisibility','off')
    end
    plot(T_F_sw,T_F_cost(round(len_color*num_rec_itr/len_color),:), 'color', 'r', 'linewidth', main_width)
    legend({'Final result'})
    title('Contribution by T_F')
    set(gca, 'xlim', [T_F_min T_F_max])
    set(gca, 'ylim', y_limit)
    line([T_F_sw(1) T_F_sw(end)], [0 0], 'color', 'k', 'linewidth', 2, 'linestyle','--','HandleVisibility','off')
    grid on
    ylabel('Est. Power [W]')
    xlabel(xlabel_T)
    nexttile(7, [1 1])
    hold on
    if SIGN_ON
        stairs(sign(alpha_hat(second_order,:)), 'linewidth', 2)
        stairs(sign(alpha_hat(first_order,:)), 'linewidth', 1.5)
        set(gca, 'ylim', [-1-sign_c 1+sign_c])
    else
        stairs(alpha_hat(second_order,:), 'linewidth', coef_width)
        ylabel('Coef. val [.]')
        yyaxis right
        stairs(alpha_hat(first_order,:), 'linewidth', coef_width, 'linestyle', coef_linestyle)
    end
    set(gca, 'xlim', [1 num_rec_itr])
    grid on
    leg = legend({sprintf('c_{%s} (T_F^2)', num2str(second_order-1)), sprintf('c_{%s} (T_F)', num2str(first_order-1))})
    set(leg, 'location', 'southoutside', 'orientation', 'horizontal')

    % Return Temperature
    first_order = 3;
    second_order = 6;
    T_R_min = 20;
    T_R_max = 60;
    T_R_sw = linspace(T_R_min, T_R_max, num_steps);
    T_R_cost = alpha_hat(first_order,:)'*T_R_sw + alpha_hat(second_order,:)'*T_R_sw.^2;

    nexttile(2, [2 1])
    hold on
    for i = 1:len_color-1
        plot(T_R_sw,T_R_cost(round(i*num_rec_itr/len_color),:), 'color', color_num(i,:),'HandleVisibility','off')
    end
    plot(T_R_sw,T_R_cost(round(len_color*num_rec_itr/len_color),:), 'color', 'r', 'linewidth', main_width)
    title('Contribution by T_R')
    set(gca, 'xlim', [T_R_min T_R_max])
    set(gca, 'ylim', y_limit)
    line([T_R_sw(1) T_R_sw(end)], [0 0], 'color', 'k', 'linewidth', 2, 'linestyle','--','HandleVisibility','off')
    grid on
    xlabel(xlabel_T)

    nexttile(8, [1 1])
    hold on
    if SIGN_ON
        stairs(sign(alpha_hat(second_order,:)), 'linewidth', 2)
        stairs(sign(alpha_hat(first_order,:)), 'linewidth', 1.5)
        set(gca, 'ylim', [-1-sign_c 1+sign_c])
    else
        stairs(alpha_hat(second_order,:), 'linewidth', coef_width)
        yyaxis right
        stairs(alpha_hat(first_order,:), 'linewidth', coef_width, 'linestyle', coef_linestyle)
    end
    set(gca, 'xlim', [1 num_rec_itr])
    grid on
    leg = legend({sprintf('c_{%s} (T_R^2)', num2str(second_order-1)), sprintf('c_{%s} (T_R)', num2str(first_order-1))})
    set(leg, 'location', 'southoutside', 'orientation', 'horizontal')
    

    % Flow
    first_order = 4;
    second_order = 7;
    m_min = 0;
    m_max = 0.5;
    m_sw = linspace(m_min, m_max, num_steps);
    m_cost = alpha_hat(first_order,:)'*m_sw + alpha_hat(second_order,:)'*m_sw.^2;
    nexttile(3, [2 1])
    hold on
    for i = 1:len_color-1
        plot(m_sw,m_cost(round(i*num_rec_itr/len_color),:), 'color', color_num(i,:))
    end
    plot(m_sw,m_cost(round(len_color*num_rec_itr/len_color),:), 'color', 'r', 'linewidth', main_width)
    title('Contribution by q')
    set(gca, 'xlim', [m_min m_max])
    set(gca, 'ylim', y_limit)
    line([m_sw(1) m_sw(end)], [0 0], 'color', 'k', 'linewidth', 2, 'linestyle','--')
    grid on
    xlabel(xlabel_q)
    
    nexttile(9, [1 1])
    hold on
    if SIGN_ON
        stairs(sign(alpha_hat(second_order,:)), 'linewidth', 2)
        stairs(sign(alpha_hat(first_order,:)), 'linewidth', 1.5)
        set(gca, 'ylim', [-1-sign_c 1+sign_c])
    else
        stairs(alpha_hat(second_order,:), 'linewidth', coef_width)
        yyaxis right
        stairs(alpha_hat(first_order,:), 'linewidth', coef_width, 'linestyle', coef_linestyle)
    end
    set(gca, 'xlim', [1 num_rec_itr])
    grid on
    leg = legend({sprintf('c_{%s} (q^2)', num2str(second_order-1)), sprintf('c_{%s} (q)', num2str(first_order-1))})
    set(leg, 'location', 'southoutside', 'orientation', 'horizontal')

    sg = sgtitle(strcat('Contributions from measurements (\delta=', delta_str{s},')'));
    sg.FontSize = get(groot, 'DefaultAxesFontSize')*1.1;
    save_plot(sprintf('coef_contribution_%d', s), 30, 18)

end


%% Functions

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Regression functions for Q and mass flow
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 1.1 PHP = f(TF, TR)
function [A, b] = std_reg_TF_TR(tab)
    N = size(tab,1);

    A1 = [tab.T_F tab.T_R];
    A2 = [tab.T_F.^2 tab.T_R.^2];
    A3 = [tab.T_F.*tab.T_R];
    A3 = [];
    A = [ones(N,1) A1 A2 A3];

    b = tab.P_HP;
end

% 1.1 PHP = f(TF, TR, TB ,mdot)
function [A, b] = std_reg_TF_TR_TB_mdot(tab)
    N = size(tab,1);
    c = 4182;
    
    % Calculate Q_HP
    Q_HP = c*tab.mdot.*(tab.T_F-tab.T_R);
    
    % Calculate COP
    COP = (tab.T_F+273.15)./(tab.T_F-tab.T_B);
    
    A1 = [tab.T_F Q_HP COP];
    A2 = [tab.T_F.^2 Q_HP.^2 COP.^2];

    A = [ones(N,1) A1 A2];

    b = tab.P_HP;
end


% Regression model based on standard data
% 2.2.1 PHP = f(TF, TR, mdot)
function [A, b] = std_reg_TF_TR_m_sep(tab)
    N = length(tab.T_F);

    A1 = [tab.T_F tab.T_R tab.mdot];
    A2 = [tab.T_F.^2 tab.T_R.^2 tab.mdot.^2];
    A = [ones(N,1) A1 A2];

    b = tab.P_HP;
end

function [A, b] = reg_mdot_u(tab)
    A1 = [tab.u1 tab.u2 tab.u3 tab.u4];
    A = [A1];
    b = tab.mdot_hat;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Finding roots in second order expression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [dotm_n, dotm_p] = mass_flow_est_2_2_1(tab, k, bias)
    N = length(tab.T_F);

    c = k(1)-tab.P_HP+bias + k(2)*tab.T_F + k(3)*tab.T_R + k(5)*tab.T_F.^2+k(6)*tab.T_R.^2;
    b = k(4);
    a = k(7);
    
    d = b.^2 - 4*a.*c;
    
    dotm_n = (-b-sqrt(d))./(2*a);
    dotm_p = (-b+sqrt(d))./(2*a);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Further functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function x = set_negative_and_off_zero(x, on)
    x(x < 0) = 0;   % Set negative values to zero
    x(~on) = 0;     % Set values where HP is off to zero
end

function E = W2Kwh(P, dt)
    E = P*dt/(3600*1000);
end

function res = RMSE(error)
    res = sqrt(mean(error.^2));
end

function res = r_squared(fit, data)
    SSR = sum((fit-mean(data)).^2);
    SST = sum((data-mean(data)).^2);
    res = SSR/SST;
end

function [axi] = create_linked_subplots_2(x, ys, xlabels, ylabels, legends, titles)
    N = length(ys);
    axi = cell(N,1);
    tiledlayout(N,1, 'Padding', 'compact', 'TileSpacing', 'compact');
    
    for i = 1:N
        axi{i} = nexttile;
        %axi{i} = subplot(N, 1, i);
        hold on
        for j = 1:size(ys{i},2)
            plot(x,ys{i}(:,j));
        end
        ylabel(ylabels{i});
        xlabel(xlabels{i});
        if ~isempty(legends{i})
            legend(legends{i});
        end
        title(titles{i});
    end
    linkaxes([axi{:}], 'x');
end

function [x] = create_regression(reg, tab_std, tab_comp, bias)
    [A_std, b_std] = reg(tab_std);
    x = A_std\b_std;
end



function [A,b] = create_recursive_data_m_2(k_m, k_u ,T)
    A_u = [T.u1 T.u2 T.u3 T.u4];
    mdot_red =  A_u*k_u;
    mdot_red(mdot_red < 0) = 0;
    T.mdot = mdot_red;
    
    [A, b] = std_reg_TF_TR_m_sep(T);
end

function plot_compare(x, fit, org)
    dt = 60;
    error = fit-org;
    N = length(error);
    rmse_val = RMSE(error);
    rmse_vec = ones(N,1)*rmse_val;
    cummulative_error = cumsum(W2Kwh(error, dt));
    figure
     ones(length(x),1)*RMSE(error);
    ys = {[fit org], [error rmse_vec -rmse_vec], [cummulative_error]};
    x_val = x;
    xlabels = {'', '','Time [days]'};
    ylabels = {'Power [W]', 'Power [W]', 'Energy [kWh]'};
    legends = {{'Fit', 'Meas'}, {'Fit-meas', 'Upper RMSE', 'Lower RMSE'}, {}};
    titles = {'Comparison between P_{HP} and Est. P_{HP}', 'Error', 'Cummulative Error'};
    axi = create_linked_subplots_2(x_val, ys, xlabels, ylabels, legends, titles);
    
    plots_2 = flip(axi{2}.Children);
    set(plots_2(2), 'linestyle', '--', 'color', 'k');
    set(plots_2(3), 'linestyle', '--', 'color', 'k');
    
    
    
    for i = 1:length(ys)
        set(axi{i}, 'Xgrid', 'on');
        set(axi{i}, 'Ygrid', 'on');
        set(axi{i}, 'xlim', [x(1) x(end)])
        leg = get(axi{i}, 'legend');
        set(leg, 'orientation', 'horizontal')
    end
end

function plot_compare_mult(x, fit, org, legs)
    dt = 60;
    error = fit-org;
    N = length(error);
    rmse_val = RMSE(error);
    rmse_vec = ones(N,1)*rmse_val;
    cummulative_error = cumsum(abs(W2Kwh(error, dt)));
    figure
     ones(length(x),1)*RMSE(error);
    ys = {[fit org], [error], [cummulative_error]};
    x_val = x;
    xlabels = {'', '','Time [days]'};
    ylabels = {'Power [W]', 'Power [W]', 'Energy [kWh]'};
    if nargin > 3
        legends = legs;
    else
        legends = {{'Fit', 'Meas'}, {'Fit-meas', 'Upper RMSE', 'Lower RMSE'}, {}};
    end
    titles = {'Comparison between P_{HP} and Est. P_{HP}', 'Error', 'Absolute Cummulative Error between Estimates and Data'};
    axi = create_linked_subplots_2(x_val, ys, xlabels, ylabels, legends, titles);
    
%     plots_2 = flip(axi{2}.Children);
%     set(plots_2(end-1), 'linestyle', '--', 'color', 'k');
%     set(plots_2(end), 'linestyle', '--', 'color', 'k');
    
    
    for i = 1:length(ys)
        set(axi{i}, 'Xgrid', 'on');
        set(axi{i}, 'Ygrid', 'on');
        set(axi{i}, 'xlim', [x(1) x(end)])
        leg = get(axi{i}, 'legend');
        set(leg, 'orientation', 'horizontal', 'location', 'northwest')
    end
end

function plot_comparison_diag_2(fit1, meas1, fit2, meas2 , title_texts, sg_title)
    limits = [0 max([meas1; fit1;meas2; fit2])+100];
    
    r2s = zeros(2,1);
    r2s(1) = r_squared(fit1, meas1);
    r2s(2) = r_squared(fit2, meas2);
    
    meas = {meas1, meas2};
    fits = {fit1, fit2};
    num_subs = 2;
    figure
    tiledlayout(1,2, 'Padding', 'compact', 'TileSpacing', 'compact');
    %tiledlayout(1,2, 'TileSpacing', 'compact'); 
    for i = 1:num_subs
        %subplot(1,num_subs,i)
        nexttile
        hold on
        line(limits,limits, 'color', 'k', 'linestyle', '--')
        plot(sort(fits{i}), sort(meas{i}), '*')
        legend({'Perfect fit line','Regression'}, 'location', 'southeast')
        set(gca, 'xlim', limits)
        set(gca, 'ylim', limits)
        xlabel('P_{HP} estimate')
        ylabel('P_{HP} data')
        title(title_texts{i})
        grid on
        text(200,3300,sprintf( '$r^2 = %.4f$',r2s(i) ), 'interpreter', 'latex', 'fontsize',23)
        daspect([1 1 1])
    end
    
    sg = sgtitle(sg_title)
    sg.FontSize = get(groot, 'DefaultAxesFontSize')*1.1;
   
end

function save_plot(plot_name, width, height)
set(gcf, 'PaperOrientation', 'landscape'); %Set the paper to have width 5 and height 5.
set(gcf, 'PaperPositionMode', 'manual');

set(gcf, 'PaperPosition', [0 0 width height]); %[left bottom width height].

set(gcf, 'PaperSize', [height-2 height]); % [width height].

plot_name = char(plot_name);
saveas(gcf, plot_name, 'pdf') %Save figure
saveas(gcf, plot_name, 'png') %Save figure
end


