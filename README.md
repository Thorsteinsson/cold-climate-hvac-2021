# Cold Climate HVAC 2021

This project contains the data and software associated to the paper published by the Authors Simon Thorsteinsson, Søren Østergaard Jensen and Jan Dimon Bendtsen at the conference Cold Climate HVAC 2021.

Sofware

Files to run
MPC
1. model_predictive_control/minlp_mpc_hp_floor_heating_example.m
2. model_predictive_control/eval_mpc_test.m

Regression analysis
regression_analysis/regression_bosch_7000_12_LWM_std_test_data.m

Dependencies
Matlab R2019b
Casadi v3.5.3
BONMIN