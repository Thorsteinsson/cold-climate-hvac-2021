clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CONFIG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SAVE_PLOTS = true;

model_file = 'water_ndays_10_dt_600_frac_0.50_alpha_0.10.mat';
val_file = 'C:\Users\MW86ZB\OneDrive - Aalborg Universitet\Dokumenter\Work\projects\opsys2\software_development\git\opsys2-simulation\simulations\sim_data_for_parameter_fitting\org_no_disturbances\analysis_first_out\house_data_no_disturbances_val_days_21.csv';
base_dt = 60;

load('alpha_hat.mat')

% Former tests
out_file = 'out_itr_70_hp_8_Hu_8_Q_15_v3.mat';
load(out_file)

rooms = [1 2 3 4];
num_rooms = length(rooms);
%Q = 15;

% MPC
%Hp = 9;

%prediction_window_hours = base_dt*Q*Hp/3600;

%Hu = Hp;

%num_itr = 70;

xlab = 'Time [hours]';
ylab_T = 'T [^\circC]';
ylab_P = 'Power [kW]';
ylab_q = 'q [kg/s]';

Tref = ones(1,500)*20;
Tref(20:50) = Tref(20:50)+2;
Tref(50:70) = Tref(50:70)-1;

val = importfile2(val_file);
val_ds = downsample_table(val, out.Q);
Ta_val = val_ds.T_ambient-273.15;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Init
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x_u_mpc = (0:1:out.num_itr-1)*out.Q*base_dt/(3600);
x_x_mpc = (0:1:out.num_itr)*out.Q*base_dt/(3600);

% Order values

ud_sim = zeros(num_rooms, out.num_itr);
%uc_sim = zeros(num_itr, 1);
for i = 1:num_rooms
    for j = 1:out.num_itr
        ud_sim(i,j) = out.ud_sol{j}(i,1); 
    end
end

u_sim = zeros(num_rooms, out.num_itr); 
for i = 1:num_rooms
    for j = 1:out.num_itr
        u_sim(i,j) = out.ud_sol{j}(i,1);
    end
end

q_est_sim = zeros(out.num_itr,1);
for i =  1:out.num_itr
    for j = 1:out.num_itr
        q_est_sim(j) = out.k_u'*u_sim(:,j);
    end
end
q_est_max = sum(out.k_u);

TF_sim = zeros(out.num_itr,1); 
P_HP_sim = zeros(out.num_itr,1); 
for j = 1:out.num_itr
    TF_sim(j) = out.uc_sol{j}(1);
    P_HP_sim(j) = out.P_HP_sol{j}(1);
end

%% (MPC) Inputs
y_plot = cell(num_rooms+1,1);
for i = 1:num_rooms
    y_plot{i} = u_sim(i,:)';
end
y_plot{end} = sum(u_sim,1)';


figure
sgtitle(sprintf('MPC Input', num2str(i)));
STAIRS = [true  true  true  true true];
x_plot = (0:1:out.num_itr-1)*out.Q*base_dt/(24*3600);
titles = {'u1', 'u2','u3','u4', 'sum u'};
xlabels = {xlab ,xlab, xlab, xlab, xlab};
ylabels = {ylab_T, ylab_T, ylab_T, ylab_T, ylab_T};
legends = {{},{},{},{},{}};
axi = create_linked_subplots(x_plot, y_plot, xlabels, ylabels, legends, titles, STAIRS);


%% (MPC) TF and TR
close all

linew = 1.5;
q_buf = 0.1;

set(0,'defaultAxesFontSize',24);
set(0,'DefaultLineMarkerSize',10);
set(0,'defaultLineLineWidth',1.5);


% Calculate return value
T_R_est_sim = zeros(out.num_itr,1);

for i = 1:out.num_itr           
    T_R_est_sim(i) = ( ...
                  ud_sim(1,i)*out.k_u(1)*(2*out.x_sim(3,1,i)-TF_sim(i))...
                + ud_sim(2,i)*out.k_u(2)*(2*out.x_sim(3,2,i)-TF_sim(i))...
                + ud_sim(3,i)*out.k_u(3)*(2*out.x_sim(3,3,i)-TF_sim(i))...
                + ud_sim(4,i)*out.k_u(4)*(2*out.x_sim(3,4,i)-TF_sim(i)))/(out.k_u'*ud_sim(:,i)+0.001);%/(k_u'*ud(:,i));
end

fig = figure;
STAIRS = [false  true, true];
x_plot = x_u_mpc;
y_plot = {[T_R_est_sim TF_sim],[(P_HP_sim+50)/1000], [q_est_sim]};
titles = {'Est. Return Temp. and Forward Temp.','Est. Consumed Power of Heat Pump', 'Est. Total Flow'};
xlabels = {{} ,{}, xlab};
ylabels = {ylab_T, ylab_P, ylab_q};
legends = {{'$\hat{T}_R$', '$T_F$', '$\overline{T}_{F}$', '$\underline{T}_{F}$'}, ...
    {'$\hat{P}_{HP}$', '$\overline{P}_{HP}$', '$\underline{P}_{HP}$'}, ...
    {'$q$', '$\overline{q}$', '$\underline{q}$'} };
axi = create_linked_subplots_2(x_plot, y_plot, xlabels, ylabels, legends, titles, STAIRS);

plot_num = 2;
set(fig, 'CurrentAxes', axi{plot_num,1});
child = get(gca, 'Children');
set(child(1), 'linewidth', linew)
line([x_u_mpc(1) x_u_mpc(end)], [out.P_HP_max out.P_HP_max]/1000, 'color', 'k', 'linestyle', '--')
line([x_u_mpc(1) x_u_mpc(end)], [out.P_HP_min out.P_HP_min]/1000, 'color', 'k', 'linestyle', '--')
leg = get(gca, 'legend');
set(gca, 'ylim', [-100/1000, (out.P_HP_max+100)/1000])
set(leg, 'String',legends{plot_num},'Interpreter','latex')
set(leg, 'location','north', 'orientation', 'horizontal')
set(gca, 'xlim', [x_u_mpc(1) x_u_mpc(end)])


plot_num = 1;
set(fig, 'CurrentAxes', axi{plot_num,1});
line([x_u_mpc(1) x_u_mpc(end)], [out.T_F_max out.T_F_max], 'color', 'k', 'linestyle', '--')
line([x_u_mpc(1) x_u_mpc(end)], [out.T_F_min out.T_F_min], 'color', 'k', 'linestyle', '--')
set(gca, 'ylim', [min(T_R_est_sim)-1 out.T_F_max+1])
leg = get(gca, 'legend');
set(leg, 'String', legends{plot_num},'Interpreter','latex')
set(leg, 'location','south', 'orientation', 'horizontal')
set(gca, 'xlim', [x_u_mpc(1) x_u_mpc(end)])

plot_num = 3;
set(fig, 'CurrentAxes', axi{plot_num,1});
child = get(gca, 'Children');
set(child(1), 'linewidth', linew);

line([x_u_mpc(1) x_u_mpc(end)], [q_est_max q_est_max], 'color', 'k', 'linestyle', '--')
line([x_u_mpc(1) x_u_mpc(end)], [0 0], 'color', 'k', 'linestyle', '--')
set(gca, 'xlim', [x_u_mpc(1) x_u_mpc(end)])
set(gca, 'ylim', [0-q_buf q_est_max+2*q_buf])

leg = get(gca, 'legend');
set(leg, 'String', legends{plot_num},'Interpreter','latex');
set(leg, 'location','northeast', 'orientation', 'horizontal');
if SAVE_PLOTS
    plot_size_w = 30;
    plot_size_h = 18;
    save_plot('mpc_forward_temp_2', plot_size_w, plot_size_h)
end

%% (MPC) Room temp simulated
close all
set(0,'defaultAxesFontSize',24);
set(0,'DefaultLineMarkerSize',10);
set(0,'defaultLineLineWidth',1.5);

buf_u = 0.1;

y_plot = cell(num_rooms,1);
titles = cell(num_rooms,1);
ylabels = cell(num_rooms,1);
legends = cell(num_rooms,1);
USE_STAIRS = zeros(num_rooms,1);
xlabels = cell(num_rooms,1);
for i = 1:num_rooms
    y_plot{i} = [squeeze(out.x_sim(:,i,:))'];
    titles{i} = sprintf('Room %d', i);
    ylabels{i} = ylab_T;
    legends{i} = {'Tr','Tf','Tw', 'T_{ref}', 'Input'};
    USE_STAIRS(i) = false;
    xlabels{i} = {};
end
xlabels{end} = xlab;
x_plot = x_x_mpc;

fig = figure;
axi = create_linked_subplots_2(x_plot, y_plot, xlabels, ylabels, legends, titles, USE_STAIRS);

for i = 1:num_rooms
    set(fig, 'CurrentAxes', axi{i,1});
    set(gca, 'ylim', [15 55]);
    children = get(gca, 'Children');
    leg = get(gca, 'legend');
    for j = 1:3
        set(children(j), 'linewidth', 1.5)
    end
    stairs(x_u_mpc, Tref(1:out.num_itr)', 'linestyle', '--', 'color', 'k', 'linewidth', 1.5)
    yyaxis right
    stairs(x_u_mpc, u_sim(i,:), 'linestyle', '-.', 'linewidth', 1.5)
    set(leg, 'String', legends{i})
    set(gca, 'ylim', [0-buf_u 1+buf_u])
    
end
sgtitle_default('Simulation results for the MPC algorithm', 1.1)

if SAVE_PLOTS
    plot_size_w = 30;
    plot_size_h = 27;
    save_plot('mpc_room_temp_2', plot_size_w, plot_size_h)
end

%% Functions

function table_ds = downsample_table(table, N)
    [M, K] = size(table);
    ds = downsample((1:1:M), N);
    table_ds = table(ds,:);
end

function [T] = sim_building_water_open(T0, dt, N, u, A, B, Bw, E, Ta)
    n = length(T0);
    T = zeros(n, N+1);
    
    T(:,1) = T0;
    
    u_on = u > 1e-5;
    
    for i = 1:N
        v = [T(:,i); u(i); Ta(i)];
        x = ode45(@(t,y) [A*y(1:end-2)+B*y(end-1) + Bw*T(3,i)*u_on(i) + E*y(end); 0; 0],[0 dt],v);
        T(:,i+1) = x.y(1:end-2,end);
    end
end

function [axi] = create_linked_subplots_2(x, ys, xlabels, ylabels, legends, titles, varargin)
    N = length(ys);
    n = length(x);
    if nargin>6
        USE_STAIRS = varargin{1};
    else
        USE_STAIRS = repmat([false], N, 1);
    end
    axi = cell(N);
    
    tiledlayout(N,1, 'Padding', 'compact', 'TileSpacing', 'compact');
    for i = 1:N
        %axi{i} = subplot(N, 1, i);
        axi{i} = nexttile;
        hold on
        for j = 1:size(ys{i},2)
            
            ysi = ys{i}(:,j);
            [ny, my] = size(ysi);
            if ny ~= n
                ysi = ysi';
            end
            if USE_STAIRS(i)
                stairs(x,ysi)
            else
                plot(x,ysi)
            end
                
        end
        ylabel(ylabels{i})
        xlabel(xlabels{i})
        if ~isempty(legends{i})
            legend(legends{i})
        end
        title(titles{i})
    end
    linkaxes([axi{:}], 'x');
end

function sgtitle_default(title_text, k)
    sg = sgtitle(title_text);
    sg.FontSize = get(0, 'DefaultAxesFontSize') * k; 
end

function save_plot(plot_name, width, height)
    set(gcf, 'PaperOrientation', 'landscape'); %Set the paper to have width 5 and height 5.
    set(gcf, 'PaperPositionMode', 'manual');

    set(gcf, 'PaperPosition', [0 0 width height]); %[left bottom width height].

    set(gcf, 'PaperSize', [height-2 height]); % [width height].

    plot_name = char(plot_name);
    saveas(gcf, plot_name, 'pdf') %Save figure
    saveas(gcf, plot_name, 'png') %Save figure
end

