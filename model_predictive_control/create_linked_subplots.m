function [axi] = create_linked_subplots(x, ys, xlabels, ylabels, legends, titles, varargin)
    STAIRS = false;
    if nargin>6
        STAIRS = varargin{1};
    end
    N = length(ys);
    n = length(x);
    axi = cell(N);
    for i = 1:N
        axi{i} = subplot(N, 1, i);
        hold on
        for j = 1:size(ys{i},2)
            
            ysi = ys{i}(:,j);
            [ny, my] = size(ysi);
            if ny ~= n
                ysi = ysi';
            end
            if STAIRS(i)
                stairs(x,ysi)
            else
                plot(x,ysi)
            end
                
        end
        ylabel(ylabels{i})
        xlabel(xlabels{i})
        if ~isempty(legends{i})
            legend(legends{i})
        end
        title(titles{i})
    end
    linkaxes([axi{:}], 'x');
end