clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CONFIG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
model_file = 'water_ndays_10_dt_600_frac_0.50_alpha_0.10.mat';
val_file = 'house_data_no_disturbances_val_days_21.csv';
base_dt = 60;

% Overall
rooms = [1 2 3 4];

% Sample time
Q = 15;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MPC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Hp = 8;         % Prediction window
Hu = Hp;        % Control window

% Constraints
% HP power
P_HP_min = 0;
P_HP_max = 4000;

% Forward temp
T_F_min = 35;
T_F_max = 55;

% Regression values
k_u = [0.138993018945279;
    0.147499055443495;
    0.025285288167092;
    0.011486321632833];

% Regression coefficients from the regression model
load('alpha_hat.mat')
c = alpha_hat(:,end)';
out.c = c;

% Initial values for the solver
Tr_initial = 20;
Tf_initial = 27;
Tw_initial = 30;

% Cost values
% Temperature reference
Tref = ones(1,500)*20;
Tref(20:50) = Tref(20:50)+2;
Tref(50:70) = Tref(50:70)-1;

% Electricity price
E_HP_cost = 0.18*ones(500,1)*Q*60/3600000;
E_HP_cost(35:45) = E_HP_cost(35:45)+3*E_HP_cost(35:45);
E_HP_cost(80:100) = E_HP_cost(80:100)+E_HP_cost(80:100);

% Number of iterations for simulation
num_itr = 5;

% Plotting
xlab = 'Time [s]';
ylab_T = 'T [C deg]';


% Solve configs
pred_windows = [Hp 3];
out.pred_windows = pred_windows;

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INIT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

num_rooms = length(rooms);

% The data contains a datapoint for each 60s
sample_rate_of_data = 60;
s_day = 86400;

val = importfile2(val_file);
load(model_file);
models = vertcat(models{:});
matrices = vertcat(models.matrices);

n = size(matrices(1).A,1);
mdis = 1;
mcon = 1;

val_ds = downsample_table(val, Q);


Ta_val = val_ds.T_ambient-273.15;

% Matrices
Ac = blkdiag(matrices(rooms).A);
Bc = blkdiag(matrices(rooms).B);
Bwc = blkdiag(matrices(rooms).Bw);
Ec = vertcat(matrices(rooms).E);

As =  matrices(rooms(1)).A;
Bs =  matrices(rooms(1)).B;
Bws = matrices(rooms(1)).Bw;
Es =  matrices(rooms(1)).E;

nc = size(Ac,1);
mcc = size(Bc,2);
mdc = size(Bc,2);

dt = sample_rate_of_data*Q; % Sample time in seconds 

x0 = repmat([18 25 30]', [num_rooms 1]);
out.x0 = x0;


% Create output vectors
T_sol = cell(num_rooms,num_itr+1);
x_sim = zeros(3,num_rooms,num_itr+1);
for i = 1:num_rooms
    x_sim(:, i, 1) = x0(n*(i-1)+1:i*n);
end

uc_sol = cell(num_itr,1);
ud_sol = cell(num_itr,1);
x_sol = cell(num_itr,1);
P_HP_sol = cell(num_itr,1);
s1_sol = cell(num_itr,1);
s2_sol = cell(num_itr,1);
s3_sol = cell(num_itr,1);
s4_sol = cell(num_itr,1);
window_len = zeros(num_itr,1);

% Store important config
out_file = sprintf('out_itr_%d_hp_%d_Hu_%d_Q_%d_v3.mat',num_itr, Hp, Hu, Q);
out.pred_windows = pred_windows;
out.E_HP_cost = E_HP_cost;
out.Tref = Tref;
out.initial.Tr_initial = Tr_initial;
out.initial.Tf_initial = Tf_initial;
out.initial.Tw_initial = Tw_initial;
out.Q = Q;
out.T_F_min = T_F_min;
out.T_F_max = T_F_max;
out.Hp = Hp;
out.P_HP_min = P_HP_min;
out.P_HP_max = P_HP_max;
out.k_u = k_u;
out.Hu = Hu;
out.num_itr = num_itr;
prediction_window_hours = base_dt*Q*Hp/3600;


%% Optimization

% Heat pump model
T_F = casadi.MX.sym('T_F',1,1);     % Discrete inputs
T_R = casadi.MX.sym('T_R',1,1);     % Continuous inputs
ud  = casadi.MX.sym('T_R',mdc,1);   % Discrete inputs
f_P_HP = casadi.Function('f_P_HP',{T_F, T_R, ud},{c(1)+c(2)*T_F+c(3)*T_R+c(4)*(k_u'*ud)+ ...
    c(5)*T_F^2+c(6)*T_R^2+c(7)*(k_u'*ud)^2}, {'T_F','T_R','ud'},{'P_HP'});
    
% States
x = casadi.MX.sym('x',nc,1);    % States
uc = casadi.MX.sym('uc',mcc,1); % Continuous inputs
ud = casadi.MX.sym('ud',mdc,1); % Discrete inputs
d = casadi.MX.sym('d',1);

% Runge-Kutta 4th order solver
f = casadi.Function('f',{x, uc, ud, d},{Ac*x + Bc*uc + Bwc*ud + Ec*d}, {'x','uc','ud','d'},{'dotx'});
k1 = f(x,uc,ud,d);
k2 = f(x + dt / 2 * k1 ,uc,ud,d);
k3 = f(x + dt / 2 * k2 ,uc,ud,d);
k4 = f(x + dt * k3 ,uc,ud,d);
xf = x + dt / 6.0 * (k1 + 2 * k2 + 2 * k3 + k4);

F = casadi.Function('F_RK4', {x, uc, ud, d}, {xf}, {'x','uc','ud', 'd'},{'x+'});

for k = 1:num_itr
    disp(sprintf('Itr: %d', k))
    SUCCESS = false;
    for j =  1:length(pred_windows)
    
        Hp = pred_windows(j);
        Hu = Hp;
    
        Nx = Hp*nc;     % Number of states (Lifted)
        Nuc = 1*Hu;     % Number of continouos inputs (Lifted)
        Ns = 5*Hu;      % Number of continues slack variables
        Nud = mcc*Hu;   % Number of discrete variables

        % Make milestones
        ENx = Nx;
        ENuc = ENx + Nuc;
        ENs = ENuc + Ns;
        ENud = ENs + Nud;

        N = Nx + Nuc + Ns + Nud;
        
        try
            clear opti
            opti = casadi.Opti();
            
            % Create large variable vector
            X = opti.variable(N,1);

            % Sort the variables into more meningful
            x = reshape(X(1:ENx), [nc Hp]);         % [Tr Tf Tw]
            uc = reshape(X(ENx+1:ENuc), [1 Hu]);    % TF
            
            % Slack variables
            s0 = reshape(X(ENuc+1:ENs), [Ns 1]);
            s = s0(1:Hu);
            s1 = s0(Hu+1:2*Hu);
            s2 = s0(2*Hu+1:3*Hu);
            s3 = s0(3*Hu+1:4*Hu);
            s4 = s0(4*Hu+1:end);
            
            % Discrete inputs
            ud = reshape(X(ENs+1:ENud), [mdc Hu]);  % v

            Ew = zeros(num_rooms,nc);
            for i = 1:num_rooms
                Ew(i,n*i) = 1;
            end

            % Calculate the multiple shooting
            for i = 1:Hp-1
                % Dynamics
                opti.subject_to(x(:,i+1)==F(x(:,i), uc(i)*ud(:,i), Ew*x(:,i).*ud(:,i), Ta_val(k+i-1)));
            end
            
            for i = 1:Hu
                        
                % Calculate return temp estimate
                T_R_v = ( k_u(1)*ud(1,i)*(2*x(3,i)-uc(i))...
                        + k_u(2)*ud(2,i)*(2*x(6,i)-uc(i))...
                        + k_u(3)*ud(3,i)*(2*x(9,i)-uc(i))...
                        + k_u(4)*ud(4,i)*(2*x(12,i)-uc(i)))/(k_u'*ud(:,i)+0.001) + s3(i);

                % Calculate regression model
                opti.subject_to(s(i) == f_P_HP(uc(i), T_R_v, ud(:,i)))

                % Add inequality constraints
                opti.subject_to(T_R_v <= (uc(i)-1)+ s4(i))
                opti.subject_to(s(i) + s1(i) >= P_HP_min)
                opti.subject_to(s(i) <= P_HP_max + s2(i))
                opti.subject_to(s1(i) >= 0)
                opti.subject_to(s2(i) >= 0)
                opti.subject_to(s3(i) >= 0)
                opti.subject_to(s4(i) >= 0)
            end

            % initial values (The measurement)
            for i = 1:num_rooms
                opti.subject_to(x(n*(i-1)+1:i*n,1) == x_sim(1:3,i,k));
            end

            % Initial guess
            if k > 1
                for i = 1:num_rooms
                    opti.set_initial(x(1+n*(i-1),:),     Tr_initial);        % Tr
                    opti.set_initial(x(2+n*(i-1),:),     Tf_initial);        % Tf
                    opti.set_initial(x(3+n*(i-1),:),     Tw_initial);        % Tw
                end
            else
                for i = 1:num_rooms
                    opti.set_initial(x(1+n*(i-1),:),     Tr_initial);        % Tr
                    opti.set_initial(x(2+n*(i-1),:),     Tf_initial);        % Tf
                    opti.set_initial(x(3+n*(i-1),:),     Tw_initial);        % Tw
                end
            end

            % Constraints on inputs
            for i = 1:Hp
                opti.subject_to(uc(:,i) >= T_F_min);
                opti.subject_to(uc(:,i) <= T_F_max);
                opti.subject_to(ud(:,i) >=0);
                opti.subject_to(ud(:,i) <= 1);
            end

            % Calculate the cost function
            room_penalty = 0;
            for  i = 1:num_rooms
                room_penalty = room_penalty + 1*Q/60*(Tref(k:k+Hp-1)-x((i-1)*n+1,:))*(Tref(k:k+Hp-1)-x((i-1)*n+1,:))';
            end

            % Collect cost-function
            opti.minimize(E_HP_cost(k:k+Hp-1)'*s + 100*ones(Hu,1)'*s1 + 100*ones(Hu,1)'*s2 + 1*ones(Hu,1)'*s3 + 100*ones(Hu,1)'*s4 + room_penalty)

            % Solve optimization problem
            opts.discrete = [zeros(1, ENs) ones(1, Nud)];
            opti.solver('bonmin', opts)
            sol = opti.solve();
            
            % If everything went well
            SUCCESS = true;
        catch exception
            disp(exception.message)
            disp('Warning: Problem could not be solved with this window')
        end
        
        if SUCCESS
            window_len(k) = Hp;
            break;
        end
    end
    
    % Store result from iteration
    uc_sol{k} = sol.value(uc);
    ud_sol{k} = sol.value(ud);
    x_sol{k} = sol.value(x);
    P_HP_sol{k} = sol.value(s);
    s1_sol{k} = sol.value(s1);
    s2_sol{k} = sol.value(s2);
    s3_sol{k} = sol.value(s3);
    s4_sol{k} = sol.value(s4);

    % Simulate
    for i = 1:num_rooms
        As =  matrices(rooms(i)).A;
        Bs =  matrices(rooms(i)).B;
        Bws = matrices(rooms(i)).Bw;
        Es =  matrices(rooms(i)).E;
        T_sol{i} = sim_building_water_open(x_sim(1:3,i,k), dt, 1, uc_sol{k}(1)'.*ud_sol{k}(i,1), As, Bs, Bws, Es, Ta_val(k:k+Hp-1));
        x_sim(1:3,i,k+1) = T_sol{i}(:,end);
    end
    P_out = P_HP_sol{k}
end

%% Store results
out.x_sim = x_sim;
out.uc_sol = uc_sol;
out.ud_sol = ud_sol;
out.x_sol = x_sol;
out.P_HP_sol = P_HP_sol;
out.s1_sol = s1_sol;
out.s2_sol = s2_sol;
out.s3_sol =  s3_sol;
out.s4_sol =  s4_sol;
save(out_file, 'out')


%% Functions
function table_ds = downsample_table(table, N)
    [M, K] = size(table);
    %cols = table.Properties.VariableNames
    ds = downsample((1:1:M), N);
    table_ds = table(ds,:);
end

function [T] = sim_building_water_open(T0, dt, N, u, A, B, Bw, E, Ta)
    n = length(T0);
    T = zeros(n, N+1);
    
    T(:,1) = T0;
    
    u_on = u > 1e-5;
    
    for i = 1:N
        v = [T(:,i); u(i); Ta(i)];
        x = ode45(@(t,y) [A*y(1:end-2)+B*y(end-1) + Bw*T(3,i)*u_on(i) + E*y(end); 0; 0],[0 dt],v);
        T(:,i+1) = x.y(1:end-2,end);
    end
end
